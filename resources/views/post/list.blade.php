@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h6 class="m-0 font-weight-bold text-primary">Data Game</h6>
            <a href="/game/create" class="btn btn-success float-right">New Data</a>
        </div>
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <th>No</th>
                    <th>Name</th>
                    <th>Developer</th>
                    <th>Year</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @forelse ($data_post as $key => $value)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->developer }}</td>
                        <td>{{ $value->year }}</td>
                        <td>
                            <a href="/game/{{ $value->id }}" class="btn btn-info">Show</a>
                            <a href="/game/{{ $value->id }}/edit" class="btn btn-primary">Edit</a>
                            <form action="/game/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                    @empty
                            
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        Swal.fire({
            title: "Berhasil!",
            text: "Memasangkan script sweet alert",
            icon: "success",
            confirmButtonText: "Cool",
        });
    </script>
@endpush