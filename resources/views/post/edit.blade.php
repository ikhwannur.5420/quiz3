@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header">
                <h2 class="card-title">Edit Post {{ $data->id }}</h2>
            </div>
            <div class="card-body">
            <form action="/game/{{ $data->id }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="name">Nama Game</label>
                <input type="text" class="form-control" name="name" id="name" value = "{{ old('name', $data->name) }}" placeholder="Masukkan Nama Game">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="developer">Developer</label>
                <input type="text" class="form-control" name="developer" id="developer" value = "{{ old('developer', $data->developer) }}" placeholder="Masukkan Nama Developer">
                @error('developer')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="developer">Tahun</label>
                <input type="number" class="form-control" name="year" id="year" value = "{{ old('year', $data->year) }}" placeholder="Masukkan Tahun Game">
                @error('year')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="gameplay">Gameplay</label>
                <textarea class="form-control" name="gameplay" id="gameplay" cols="30" rows="10">{{ old('gameplay', $data->gameplay) }}</textarea>
                @error('gameplay')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
            </div>
        </div>
    </div>
</div>
@endsection