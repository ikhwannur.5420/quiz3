@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header">
                <h2 class="card-title">Tampilkan Data {{ $data->name }}</h2>
            </div>
            <div class="card-body">
                @csrf
                <div class="form-group">
                    <h2 class="text-primary">{{ $data->name }} ({{ $data->year }})</h2>
                </div>
                <div class="form-group">
                    <p>Developer : {{ $data->developer }}</p>
                </div>
                <div class="form-group">
                    <h3 for="gameplay">Gameplay</h3>
                    <p>{{ $data->gameplay }}</p>
                </div>
                <div class="form-group">
                    <p for="gameplay">Platform</p>
                    <div class="inline">
                        @forelse ($data_platform as $key => $value)
                            <button class="btn btn-sm btn-primary">{{ $value->name }}</button>
                        @empty
                            
                        @endforelse
                        
                        {{-- <button class="btn btn-sm btn-primary">MacOs</button>
                        <button class="btn btn-sm btn-primary">Linux</button>
                        <button class="btn btn-sm btn-primary">Xbox360</button>
                        <button class="btn btn-sm btn-primary">Ps4</button> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection