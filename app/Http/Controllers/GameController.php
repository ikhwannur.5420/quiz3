<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

DB::enableQueryLog();

use Illuminate\Support\Arr;

class GameController extends Controller
{
    public function index()
    {
        $data_post = DB::table('game')->get();
        return view('post.list', compact('data_post'));
    }

    public function create()
    {
        $data_platform = DB::table('platform')->get();
        return view('post.create', compact('data_platform'));
    }

    public function store(Request $request)
    {
        // dd($request->platform);
        $request->validate([
            'name' => 'required|unique:game',
            'developer' => 'required',
            'year' => 'required',
            'gameplay' => 'required',
            'platform' => 'required'
        ]);

        $query = DB::table('game')->insert([
            "name" => $request["name"],
            "gameplay" => $request["gameplay"],
            "developer" => $request["developer"],
            "year" => $request["year"],
            "user_id" => 1,
            "platform" => implode(',', $request->platform)
        ]);

        return redirect('/game')->with('success', 'Data berhasil disimpan!');
    }

    public function show($post_id)
    {
        $data = DB::table('game')->where('id', $post_id)->first();
        $data_idplatform = explode(',', $data->platform);
        $data_platform = DB::table('platform')->whereIn('id', $data_idplatform)->get();
        return view('post.show', compact('data', 'data_platform'));
    }

    public function edit($post_id)
    {
        $data = DB::table('game')->where('id', $post_id)->first();
        return view('post.edit', compact('data'));
    }

    public function update($post_id, Request $request)
    {
        $request->validate([
            'name' => 'required|unique:game',
            'developer' => 'required',
            'year' => 'required',
            'gameplay' => 'required',
        ]);

        $query = DB::table('game')->where('id', $post_id)->update(
            [
                "name" => $request["name"],
                "gameplay" => $request["gameplay"],
                "developer" => $request["developer"],
                "year" => $request["year"],
                "user_id" => 1
            ]
        );

        return redirect('/game')->with('success', 'Data berhasil diupdate!');
    }

    public function destroy($post_id)
    {
        DB::table('game')->where('id', $post_id)->delete();
        return redirect('/game')->with('success', 'Data berhasil dihapus!');
    }
}
